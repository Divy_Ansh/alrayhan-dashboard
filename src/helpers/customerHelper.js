import { API } from "../Backend";

// getCustomers
export const getCustomers = () => {
  return fetch(`${API}/api/admin/get/all/users`, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  })
    .then((response) => response.json())
    .catch((err) => console.error(err));
};

// updateUserAccountStatus
export const updateUserAccountStatus = (userId) => {
  return fetch(`${API}/api/admin/user/${userId}/update/account/status`, {
    method: "GET",
    headers: {
      Accept: "application/json",
    },
  })
    .then((response) => response.json())
    .catch((err) => console.error(err));
};
