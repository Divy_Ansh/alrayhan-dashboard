import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import ScrollableFeed from "react-scrollable-feed";

import { Select, MenuItem } from "@material-ui/core";
import { updateOrderStatus } from "../../helpers/orderHelper";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.grey[300],
    color: theme.palette.common.black,
    padding: "16px !important",
    fontSize: "15px",
  },
  body: {
    fontSize: "14px",
  },
  root: {
    padding: "4px 16px",
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function CustomizedTables(props) {
  const classes = useStyles();

  // changeHandler
  const changeHandler = (orderId) => (e) => {
    let status = e.target.value;
    updateOrderStatus(orderId, status)
      .then((data) => {
        console.log(data);
        props.setReload(!props.Reload);
        props.toast.success("Status Updated!");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              {props.columns.map((column, index) => {
                return (
                  <StyledTableCell key={index} align="center">
                    {column}
                  </StyledTableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.rows &&
              props.rows.map((row) => (
                <TableRow key={row._id}>
                  <TableCell component="th" align="center" scope="row">
                    {row.id}
                  </TableCell>
                  <TableCell component="th" align="center" scope="row">
                    {row.currency && row.currency / 100}
                  </TableCell>
                  <TableCell component="th" align="center" scope="row">
                    {row.amount}
                  </TableCell>
                  <TableCell component="th" align="center" scope="row">
                    {row.status}
                  </TableCell>
                  <TableCell component="th" align="center" scope="row">
                    {row.payment_method_types[0]}
                  </TableCell>
                  <TableCell component="th" align="center" scope="row">
                    {row.description}
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      {props.rows.length == 0 && (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "100px",
            fontSize: 18,
          }}
        >
          <h3>Currently Empty</h3>
        </div>
      )}
    </>
  );
}
