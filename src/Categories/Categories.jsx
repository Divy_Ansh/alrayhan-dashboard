import React, { useState, useEffect } from "react";
import Modal from "react-modal";

// css
import "./Categories.css";

// helper functions
import {
  getCategories,
  deleteCategory,
  updateCategory,
  createCategory,
} from "../../helpers/categoryHelper";
import { isAuthenticated } from "../../helpers/AuthHelper";

// components
import Sidebar from "../Sidebar/Sidebar";
import Header from "../header/Header";
import Loader from "../../Loader/Loader";
import { TextField, IconButton, Select, InputLabel } from "@material-ui/core";

import AddModal from "../Modal/Modal";

// Table
import Table from "../../Tables/Categories/CategoriesTable";

// notifications
import { toast, Toaster } from "react-hot-toast";

// styles for modal
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    width: "30%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

// element for modal
Modal.setAppElement("#root");

const Offers = () => {
  // AdminId or UserId
  const userId = isAuthenticated().data.user._id;

  // useState hooks
  const [Categories, setCategories] = useState([]);
  const [Search, setSearch] = useState("");
  const [Loading, setLoading] = useState(true);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [Reload, setReload] = useState(false);
  const [Values, setValues] = useState({
    categoryName: "",
    status: true,
  });

  // for edit
  const [Edit, setEdit] = useState("");
  const [EditModalOpen, setEditModalOpen] = useState(false);
  const [EditValues, setEditValues] = useState({
    categoryName: "",
    status: "",
  });

  // destructuring
  const { categoryName, status } = Values;

  // getOffers method
  const getAllCategories = () => {
    getCategories()
      .then((data) => {
        console.log("OFFERS", data);
        setCategories(data);
        setLoading(false);
      })
      .catch((err) => {
        console.log("ERROR", err);
        setLoading(false);
      });
  };

  // useEffect
  useEffect(() => {
    getAllCategories();
  }, [Reload]);

  // changeHandler
  const changeHandler = (name) => (e) => {
    let value = e.target.value;
    setValues({ ...Values, [name]: value });
  };

  // addCategoryHandler
  const addCategoryHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    createCategory(Values)
      .then((data) => {
        if (data.error) {
          setLoading(false);
          toast.error(data.message);
          return "";
        }
        setLoading(false);
        setReload(!Reload);
        setIsOpen(false);
        toast.success(data.message);
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  };

  // updateCategoryHandler
  const updateCategoryHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    updateCategory(Edit._id, EditValues)
      .then((data) => {
        if (data.error) {
          setLoading(false);
          toast.error("Failed to Update!");
          return "";
        }
        console.log(data);
        setEditModalOpen(false);
        setReload(!Reload);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  // deleteCategoryHandler
  const deleteCategoryHandler = (_id) => {
    deleteCategory(_id)
      .then((data) => {
        console.log(data);
        toast.success("Deleted Successfully!");
        setReload(!Reload);
      })
      .catch((err) => console.error(err));
  };

  // modal close functions
  const closeModal = () => {
    setIsOpen(false);
  };

  const closeEditModal = () => {
    setEditModalOpen(false);
  };

  //* offers return
  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div style={{ padding: "0" }} className="col-md-2">
            <Sidebar />
          </div>
          {/* Conditional rendering */}
          {Loading ? (
            <div
              className="col-md-10"
              style={{
                height: "90vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Loader />
            </div>
          ) : (
            <div className="col-md-10">
              <Toaster />
              <Header />
              <div className="header-wrapper">
                <h3>Categories</h3>
                <button
                  onClick={() => setIsOpen(true)}
                  className="addCategory-btn"
                >
                  Add Category
                </button>
              </div>

              {/* MODAL for Adding*/}
              <AddModal modalIsOpen={modalIsOpen} setIsOpen={setIsOpen}>
                <form onSubmit={addCategoryHandler}>
                  <h3 className="text-center">Add Category</h3>
                  <TextField
                    style={{ width: "100%" }}
                    id="standard-basic"
                    label="Category Name"
                    defaultValue={categoryName}
                    onChange={changeHandler("categoryName")}
                  />
                  <br />
                  <br />

                  <InputLabel id="demo-simple-select-label">Status</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    defaultValue={status}
                    onChange={changeHandler("status")}
                  >
                    <option value="true">True</option>
                    <option value="false">False</option>
                  </Select>
                  <br />
                  <br />

                  <Button type="submit" variant="contained" color="primary">
                    Add New Category
                  </Button>
                </form>
              </AddModal>
              {/*Adding MODAL END */}

              {/* Modal for Edit */}
              {Edit && (
                <Modal
                  isOpen={EditModalOpen}
                  onRequestClose={closeEditModal}
                  style={customStyles}
                  contentLabel="Example Modal"
                >
                  {/* Form Start */}
                  <form onSubmit={updateCategoryHandler}>
                    <h3 className="text-center">Update Category</h3>
                    <TextField
                      style={{ width: "100%" }}
                      id="standard-basic"
                      label="Category Name"
                      defaultValue={Edit.categoryName}
                      onChange={(e) =>
                        setEditValues({
                          ...EditValues,
                          categoryName: e.target.value,
                        })
                      }
                    />
                    <br />
                    <br />

                    <InputLabel id="demo-simple-select-label">
                      Status
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      defaultValue={Edit.status}
                      onChange={(e) =>
                        setEditValues({
                          ...EditValues,
                          status: e.target.value,
                        })
                      }
                    >
                      <option value="true">True</option>
                      <option value="false">False</option>
                    </Select>
                    <br />
                    <br />

                    <Button type="submit" variant="contained" color="primary">
                      Update Category
                    </Button>
                  </form>
                  {/* Form End */}
                </Modal>
              )}
              {/* Edit Modal End */}

              <br />
              <Table
                Delete={(_id) => {
                  deleteCategoryHandler(_id);
                }}
                Edit={(data) => {
                  console.log("DATA", data);
                  setEdit(data);
                  setEditValues({
                    categoryName: data.categoryName,
                    status: data.status,
                  });
                  setEditModalOpen(true);
                }}
                search={Search}
                rows={Categories}
                columns={["ID", "Name", "Status", "Actions"]}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Offers;
