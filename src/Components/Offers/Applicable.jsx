import React from "react";
import styles from "./Applicable.module.css";

// material-components
import { Radio, TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";

// helpers
import { getAllItems } from "../../helpers/itemHelper";

import toast from "react-hot-toast";

const Applicable = ({ Data, Values, setValues, setLoading }) => {
  const { itemsList, categoriesList } = Data;

  const { applicableFor, applicableItemList, applicableCategoryList } = Values;

  return (
    <div className={styles.compContent}>
      <h3>Applicable for: </h3>

      <div className={styles.formGroup}>
        <div className={styles.selectors}>
          <div className={styles.radio}>
            <p>All Products</p>
            <Radio
              checked={applicableFor == "ALL-PRODUCTS"}
              onChange={(e) =>
                setValues({
                  ...Values,
                  applicableFor: "ALL-PRODUCTS",
                  applicableItemList: itemsList,
                })
              }
            />
          </div>

          <div className={styles.radio}>
            <p>Specific Products</p>
            <Radio
              checked={applicableFor == "SPECIFIC-PRODUCTS"}
              onChange={(e) =>
                setValues({ ...Values, applicableFor: "SPECIFIC-PRODUCTS" })
              }
            />
          </div>

          <div className={styles.radio}>
            <p>Specific Category</p>
            <Radio
              checked={applicableFor == "SPECIFIC-CATEGORY"}
              onChange={(e) =>
                setValues({ ...Values, applicableFor: "SPECIFIC-CATEGORY" })
              }
            />
          </div>
        </div>

        {applicableFor == "SPECIFIC-PRODUCTS" && (
          <Autocomplete
            multiple
            id="tags-standard"
            options={itemsList ? itemsList : []}
            getOptionLabel={(option) => option.name}
            className={styles.textField}
            value={applicableItemList}
            onChange={(e, val) =>
              setValues({ ...Values, applicableItemList: val })
            }
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Select Product"
                placeholder="Select Product"
              />
            )}
          />
        )}

        {applicableFor == "SPECIFIC-CATEGORY" && (
          <Autocomplete
            multiple
            id="tags-standard"
            options={categoriesList ? categoriesList : []}
            getOptionLabel={(option) => option.subCategoryName}
            className={styles.textField}
            value={applicableCategoryList}
            onChange={(e, val) =>
              setValues({ ...Values, applicableCategoryList: val })
            }
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Select Category"
                placeholder="Select Product"
              />
            )}
          />
        )}
      </div>
    </div>
  );
};

export default Applicable;
