import React from "react";
import styles from "./CustomerGroup.module.css";

// material-ui
import { Radio, TextField } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";

const CustomerType = ({ Data, Values, setValues }) => {
  // Destructuring

  const { usersList } = Data;

  const { customerType, customerGroup, basicUserList, customerList } = Values;

  return (
    <div className={styles.compContent}>
      <h3>Customer Type: </h3>

      <div className={styles.formGroup}>
        <div className={styles.selectors}>
          <div className={styles.radio}>
            <p>All Customers</p>
            <Radio
              checked={customerType == "ALL-CUSTOMERS"}
              onChange={(e) =>
                setValues({ ...Values, customerType: "ALL-CUSTOMERS" })
              }
            />
          </div>

          <div className={styles.radio}>
            <p>Specific Customers</p>
            <Radio
              checked={customerType == "CUSTOMERS"}
              onChange={(e) =>
                setValues({ ...Values, customerType: "CUSTOMERS" })
              }
            />
          </div>
        </div>

        {customerType == "CUSTOMERS" && (
          <Autocomplete
            multiple
            id="tags-standard"
            options={usersList ? usersList : []}
            getOptionLabel={(option) => option.name}
            className={styles.textField}
            value={customerList}
            onChange={(e, val) => setValues({ ...Values, customerList: val })}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Select Customer List"
                placeholder="Select Customer List"
                // error={error.customerList}
                // helperText={error.customerList}
              />
            )}
          />
        )}
      </div>
    </div>
  );
};

export default CustomerType;
