import React, { useState, useEffect } from "react";

import {
  TextField,
  Select,
  Button,
  InputLabel,
  MenuItem,
} from "@material-ui/core";

// router
import { useParams } from "react-router-dom";

// components
import Loader from "../../Loader/Loader";
import toast, { Toaster } from "react-hot-toast";

// helpers
import { getAllSubCategories } from "../../helpers/categoryHelper";
import { getItem, updateItem } from "../../helpers/itemHelper";
import { useHistory } from "react-router";

const ProductDetails = ({ CurrentPage, setCurrentPage }) => {
  const [SubCategories, setSubCategories] = useState([]);
  const [Loading, setLoading] = useState(true);
  const [Reload, setReload] = useState(false);
  const [Values, setValues] = useState({
    name: "",
    status: true,
    amount: "",
    description: "",
    photo: "",
    category: "",
    nameArabic: "",
    descriptionArabic: "",
    formData: "",
  });

  //? itemId
  const { itemId } = useParams();
  console.log("ITEM ID", itemId);

  const getItemDetails = () => {
    setLoading(true);

    getItem(itemId)
      .then((data) => {
        console.log("ITEM", data);
        const { amount, description, descriptionArabic, name, nameArabic } =
          data;
        setValues({
          ...Values,
          name: name ? name : "",
          status: status ? status : "",
          amount: amount ? amount : "",
          description: description ? description : "",
          category: data.category ? data.category._id : data.subCategory._id,
          nameArabic: nameArabic ? nameArabic : "",
          descriptionArabic: descriptionArabic ? descriptionArabic : "",
          formData: new FormData(),
        });
        setLoading(false);
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  };

  useEffect(() => {
    getItemDetails();
  }, [Reload]);

  // destructuring
  const {
    name,
    status,
    amount,
    description,
    photo,
    category,
    formData,
    nameArabic,
    descriptionArabic,
  } = Values;

  // changeHandler
  const changeHandler = (name) => (e) => {
    let value = name === "photo" ? e.target.files[0] : e.target.value.trim();

    formData.set(name, value);
    setValues({ ...Values, [name]: value });
  };

  // submitHandler
  const submitHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    updateItem(itemId, formData)
      .then((data) => {
        console.log("UPDATE ITEM RES", data);

        setValues({
          name: "",
          status: true,
          amount: "",
          description: "",
          photo: "",
          category: "",
          formData: "",
        });

        setReload(!Reload);
        setLoading(false);
        toast.success("Item Udpated!");
        setCurrentPage("Attributes");
      })
      .catch((err) => {
        console.error(err);

        setLoading(true);
      });
  };

  console.log(Values);

  // component return
  return (
    <div>
      {Loading ? (
        <div
          style={{
            width: "100%",
            minHeight: "60vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="col-md-10"
        >
          <Loader />
        </div>
      ) : (
        <div>
          <Toaster />
          <form
            onSubmit={submitHandler}
            style={{
              width: "100%",
              margin: "auto",
              boxShadow: "0 0 4px rgba(0,0,0,0.4)",
              padding: "10px",
              marginTop: "10px",
            }}
          >
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <TextField
                style={{ width: "60%" }}
                id="standard-basic"
                label="Product Name"
                defaultValue={name}
                onChange={changeHandler("name")}
              />

              <TextField
                style={{ width: "60%", marginLeft: "20px" }}
                id="standard-basic"
                label="Short Description"
                defaultValue={description}
                onChange={changeHandler("description")}
              />
            </div>
            <br />
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <TextField
                style={{ width: "60%" }}
                id="standard-basic"
                label="Product Name (in arabic)"
                defaultValue={nameArabic}
                onChange={changeHandler("nameArabic")}
              />

              <TextField
                style={{ width: "60%", marginLeft: "20px" }}
                id="standard-basic"
                label="Short Description (in arabic)"
                defaultValue={descriptionArabic}
                onChange={changeHandler("descriptionArabic")}
              />
            </div>
            <br />
            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  width: "50%",
                  display: "flex",
                  justifyContent: "space-around",
                  alignItems: "center",
                }}
              >
                {/*Sub Category */}
                {/* <div>
                  <InputLabel htmlFor="age-native-simple">
                    Sub Category
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    defaultValue={category}
                    onChange={changeHandler("category")}
                  >
                    {SubCategories &&
                      SubCategories.map((cate) => {
                        const { _id, subCategoryName } = cate;
                        return (
                          <MenuItem key={_id} value={_id}>
                            {subCategoryName}
                          </MenuItem>
                        );
                      })}
                  </Select>
                </div> */}
                {/* Status */}
                <div>
                  <InputLabel htmlFor="age-native-simple">Status</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    defaultValue={status}
                    onChange={changeHandler("status")}
                  >
                    <MenuItem value="true">True</MenuItem>
                    <MenuItem value="false">False</MenuItem>
                  </Select>
                </div>

                <Button variant="contained" component="label">
                  {photo ? `${photo.name}` : "Upload Image"}
                  <input onChange={changeHandler("photo")} type="file" hidden />
                </Button>
                <br />
              </div>

              <TextField
                type="number"
                style={{ width: "50%", marginLeft: "20px" }}
                id="standard-basic"
                label="Amount"
                defaultValue={amount}
                onChange={changeHandler("amount")}
              />
            </div>
            <br />
            {/* Submit Button */}
            <div className="text-center mt-3">
              <button
                type="submit"
                style={{
                  padding: "15px",
                  width: "30%",
                  height: "fit-content",
                  backgroundColor: "black",
                  color: "white",
                  border: "none",
                  position: "absolute",
                  left: "42.57%",
                  right: "23.54%",
                  top: "80.55%",
                  bottom: "3.92%",

                  background: "#00A0A7",
                  borderRadius: "5px",
                }}
              >
                Save Changes and Next
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
};

export default ProductDetails;
