import React, { useState } from "react";
import "./Items.css";

import toast from "react-hot-toast";

// material-ui
import {
  RadioGroup,
  FormControlLabel,
  Radio,
  TextField,
} from "@material-ui/core";

// helpers
import { addDiscountInItem } from "../../helpers/itemHelper";

const Discount = ({ setCurrentPage }) => {
  const [Loading, setLoading] = useState(false);
  const [DiscountedPeriod, setDiscountedPeriod] = useState("");
  const [selectedDate, setSelectedDate] = React.useState(
    new Date().toISOString().slice(0, 10)
  );
  const [Values, setValues] = useState({
    price: "",
    period: "",
  });

  // destructuring
  const { price, period } = Values;

  // changeHandler
  const changeHandler = (name) => (e) => {
    let value = e.target.value;

    setValues({ ...Values, [name]: value });
  };

  // submitHandler
  const submitHandler = (e) => {
    e.preventDefault();

    if (!localStorage.getItem("Item")) {
      setLoading(false);
      toast.error("Please make one item first!");
      return;
    }

    if (DiscountedPeriod == "always") {
      setValues({ ...Values, period: "always" });
    }

    setLoading(true);

    let item = JSON.parse(localStorage.getItem("Item"));
    let { _id } = item;

    setValues({ ...Values });

    addDiscountInItem(_id, Values)
      .then((data) => {
        if (data.error) {
          setLoading(false);
          toast.error(data.message);
          return;
        }
        console.log("DISCOUNT RESPONSE", data);
        localStorage.removeItem("Item");
        setCurrentPage("Product Details");
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        toast.error("Something went wrong!");
      });
  };

  // discountPeriodHandler
  const discountPeriodHandler = (e) => {
    let value = e.target.value;

    if (value === "always") {
      setValues({ ...Values, period: value });
    } else {
      setValues({ ...Values, period: "" });
      setDiscountedPeriod("upto");
      return;
    }
  };

  console.log(Values);

  return (
    <>
      <div className="discount mt-3 p-3">
        <form>
          <h3>
            Discounted Price <span>(%)</span>
          </h3>
          <input
            placeholder="e.g - 10%"
            defaultValue={price}
            onChange={changeHandler("price")}
            type="number"
            style={{ padding: "10px" }}
          />
          <h3 className="mt-5">Discounted Period</h3>
          <RadioGroup
            aria-label="gender"
            name="gender1"
            onChange={discountPeriodHandler}
          >
            <FormControlLabel
              value="always"
              control={<Radio />}
              label="Always"
            />
            <FormControlLabel value="upto" control={<Radio />} label="Upto" />
          </RadioGroup>
          {DiscountedPeriod === "upto" && (
            <TextField
              id="date"
              label="Birthday"
              type="date"
              min={selectedDate}
              value={selectedDate}
              onChange={(e) => setValues({ ...Values, period: e.target.value })}
              InputLabelProps={{
                shrink: true,
              }}
            />
          )}
        </form>
      </div>

      <div style={{ textAlign: "center" }}>
        <button
          onClick={submitHandler}
          type="submit"
          // style={{
          //   padding: "15px",
          //   width: "40%",
          //   margin: "50px auto",
          //   backgroundColor: "black",
          //   color: "white",
          //   border: "none",
          // }}
          style={{
            padding: "10px",
            width: "30%",
            margin: "40px auto",
            backgroundColor: "black",
            color: "white",
            border: "none",
            position: "absolute",
            left: "42.57%",
            right: "23.54%",
            top: "80.55%",
            bottom: "3.92%",

            background: "#00A0A7",
            borderRadius: "5px",
          }}
        >
          Save Changes and Next
        </button>
      </div>
    </>
  );
};

export default Discount;
