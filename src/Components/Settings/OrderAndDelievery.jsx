import React, { useState } from "react";

import Loader from "../../Loader/Loader";

// material-ui
import { FormHelperText } from "@material-ui/core";
import toast, { Toaster } from "react-hot-toast";

// helpers
import { addTiming } from "../../helpers/restraurantHelper";
import { Redirect } from "react-router-dom";

const OrderAndDelivery = ({ CurrentPage, setCurrentPage }) => {
  // useState hooks
  const [Loading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [FormError, setFormError] = useState({
    prepTime: false,
    minOrder: false,
    note: false,
  });
  const [Values, setValues] = useState({
    prepTime: "",
    minOrder: "",
    note: "",
  });

  const { prepTime, minOrder, note } = Values;

  // changeHandler
  const changeHandler = (name) => (e) => {
    let value = e.target.value.toLowerCase();
    setValues({ ...Values, [name]: value });
  };

  // submitHandler
  const submitHandler = (e) => {
    e.preventDefault();

    if (!localStorage.getItem("RestroId")) {
      setValues({
        prepTime: "",
        minOrder: "",
        note: "",
      });
      setLoading(false);
      toast.error("Please Add One Restraurant First!");
      return;
    }

    if (Validate()) {
      setLoading(true);

      if (!prepTime || !minOrder) {
        setLoading(false);
        toast.error("Please include all fields first!");
        return;
      }

      let restro = JSON.parse(localStorage.getItem("RestroId"));
      const { _id } = restro;

      console.log(restro);
      console.log(_id);

      addTiming(_id, Values)
        .then((data) => {
          if (data.error) {
            setLoading(false);
            toast.error(data.message);
            return;
          }
          console.log(data);
          localStorage.removeItem("RestroId");
          setLoading(false);
          setRedirect(true);
        })
        .catch((err) => {
          console.log(err);
          setLoading(false);
          toast.error("Something went wrong!");
        });
    }
  };

  // redirectHandler
  const redirectHandler = () => {
    if (redirect) {
      return <Redirect to="/admin/dashboard/setting" />;
    }
  };

  const Validate = () => {
    let value = true;

    let err = {
      prepTime: false,
      minOrder: false,
      note: false,
    };

    if (prepTime == "") {
      value = false;
      err.prepTime = "Enter Prep Time First!";
    } else if (prepTime < 0) {
      value = false;
      err.prepTime = "Enter Valid Prep Time!";
    }

    if (minOrder == "") {
      value = false;
      err.minOrder = "Enter Min Order Price First!";
    } else if (minOrder < 0) {
      value = false;
      err.minOrder = "Enter Valid Min Order!";
    }

    setFormError({ ...err });
    return value;
  };

  console.log("FormError", FormError);

  // component return
  return (
    <div>
      {Loading ? (
        <div
          style={{
            width: "100%",
            minHeight: "60vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="col-md-10"
        >
          <Loader />
        </div>
      ) : (
        <>
          <Toaster />
          {redirectHandler()}
          <div
            style={{
              width: "100%",
              margin: "20px auto",
              padding: "25px",
              boxShadow: "0 0 4px rgba(0,0,0,0.4)",
            }}
          >
            <form>
              <div
                style={{
                  marginBottom: "20px",
                  display: "flex",
                  justifyContent: "space-around",
                }}
              >
                <div style={{ width: "48%", borderRadius: "5px" }}>
                  <input
                    placeholder={
                      FormError.prepTime
                        ? FormError.prepTime
                        : "Prep Time (minutes)"
                    }
                    style={{
                      padding: "15px",
                      border: "1px solid #888888",
                      outline: "none",
                      borderRadius: "5px",
                      width: "100%",
                    }}
                    className={FormError.prepTime ? "border border-danger" : ""}
                    type="number"
                    onChange={changeHandler("prepTime")}
                    defaultValue={prepTime}
                  />
                  <FormHelperText error={FormError.prepTime}>
                    {FormError.prepTime}
                  </FormHelperText>
                </div>

                <div style={{ width: "48%", borderRadius: "5px" }}>
                  <input
                    style={{
                      padding: "15px",
                      border: "1px solid #888888",
                      outline: "none",
                      borderRadius: "5px",
                      width: "100%",
                    }}
                    placeholder={
                      FormError.minOrder
                        ? FormError.minOrder
                        : "Min Order Price"
                    }
                    className={FormError.minOrder ? "border border-danger" : ""}
                    type="number"
                    onChange={changeHandler("minOrder")}
                    defaultValue={minOrder}
                  />

                  <FormHelperText error={FormError.minOrder}>
                    {FormError.minOrder}
                  </FormHelperText>
                </div>
              </div>
              <div>
                <input
                  placeholder="Note to Rider "
                  style={{
                    padding: "15px",
                    border: "1px solid #888888",
                    outline: "none",
                    borderRadius: "5px",
                    width: "48%",
                    marginLeft: "1%",
                  }}
                  type="text"
                  onChange={changeHandler("note")}
                  defaultValue={note}
                />
              </div>
            </form>
          </div>

          <div style={{ marginTop: "50px", textAlign: "center" }}>
            <button
              onClick={submitHandler}
              style={{
                padding: "10px",
                width: "30%",
                margin: "40px auto",
                backgroundColor: "black",
                color: "white",
                border: "none",
                // position: "absolute",
                left: "42.57%",
                right: "23.54%",
                top: "80.55%",
                bottom: "3.92%",
                fontSize: "20px",

                background: "#00A0A7",
                borderRadius: "5px",
              }}
            >
              Save
            </button>
          </div>
        </>
      )}
    </div>
  );
};

export default OrderAndDelivery;
