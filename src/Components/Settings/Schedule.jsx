import React, { useState } from "react";

import Loader from "../../Loader/Loader";

// material-ui
import toast, { Toaster } from "react-hot-toast";
import { Redirect } from "react-router";

// helpers
import { addRestroSchedule } from "../../helpers/restraurantHelper";

const Schedule = ({ setCurrentPage }) => {
  // useState hooks
  const [Loading, setLoading] = useState(false);
  const [schedule, setschedule] = useState([]);
  const [redirect, setRedirect] = useState(false);

  // updateSchedule
  const updateSchedule = (day, { startTime, endTime }) => {
    console.log("SCHEDULE", schedule);

    const index = schedule.findIndex((item) => item.day === day);

    if (index >= 0) {
      let update = schedule;
      if (startTime) update[index].startTime = startTime;
      if (endTime) update[index].endTime = endTime;
      setschedule(update);
      return "";
    }

    const updateArr = schedule;
    // Find if the day alredy exists
    // If exists, update startTime, endTime

    const update = { day };
    if (startTime) update.startTime = startTime;
    if (endTime) update.endTime = endTime;
    // if not exists, push the update

    updateArr.push(update);
    setschedule(updateArr);
  };

  // submitHandler
  const submitHandler = (e) => {
    e.preventDefault();

    setLoading(true);

    if (!localStorage.getItem("RestroId")) {
      setLoading(false);
      toast.error("Please Add One Restraurant First!");
      return;
    }

    let restro = JSON.parse(localStorage.getItem("RestroId"));
    let { _id } = restro;

    console.log("RESTRO", restro);
    console.log("RESTRO ID", _id);

    if (schedule.length == 0) {
      setLoading(false);
      toast.error("Schedule Can't be empty!");
      return;
    }

    addRestroSchedule(_id, schedule)
      .then((data) => {
        console.log(data);
        setLoading(false);
        toast.success("Schedule Added!");
        setCurrentPage("Timing");
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        toast.error("Something went wrong!");
      });
  };

  // component return
  return (
    <div>
      {Loading ? (
        <div
          style={{
            width: "100%",
            minHeight: "60vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="col-md-10"
        >
          <Loader />
        </div>
      ) : (
        <>
          <Toaster />
          <div
            style={{
              width: "80%",
              margin: "20px auto",
              padding: "20px",
              boxShadow: "0 0 4px rgba(0,0,0,0.4)",
            }}
          >
            <h4 style={{ textAlign: "center", marginBottom: "50px" }}>
              Set Time Schedule
            </h4>

            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <h3 className="col-sm-3">Monday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
                onChange={(e) =>
                  updateSchedule("Monday", {
                    startTime: e.target.value.toLowerCase(),
                  })
                }
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
                onChange={(e) =>
                  updateSchedule("Monday", {
                    endTime: e.target.value.toLowerCase(),
                  })
                }
              />
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
              }}
            >
              <h3 className="col-sm-3">Tuesday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
                onChange={(e) =>
                  updateSchedule("Tuesday", { startTime: e.target.value })
                }
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
                onChange={(e) =>
                  updateSchedule("Tuesday", { endTime: e.target.value })
                }
              />
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
              }}
            >
              <h3 className="col-sm-3">Wednesday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
              />
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
              }}
            >
              <h3 className="col-sm-3">Thursday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
              />
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
              }}
            >
              <h3 className="col-sm-3">Friday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
              />
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
              }}
            >
              <h3 className="col-sm-3">Saturday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
              />
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "15px",
                justifyContent: "space-around",
              }}
            >
              <h3 className="col-sm-3">Sunday </h3>
              <input
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="Start Time"
                type="text"
                className="col-sm-3"
              />
              <input
                className="col-sm-3"
                style={{
                  padding: "15px",
                  borderRadius: "10px",
                  outline: "none",
                  border: "1px solid #888888",
                }}
                placeholder="End Time"
                type="text"
              />
            </div>
            <div style={{ marginTop: "50px", textAlign: "center" }}>
              <button
                onClick={submitHandler}
                style={{
                  padding: "25px",
                  width: "30%",
                  margin: "40px auto",
                  backgroundColor: "black",
                  color: "white",
                  border: "none",
                  // position: "absolute",
                  left: "42.57%",
                  right: "23.54%",
                  top: "80.55%",
                  bottom: "3.92%",

                  background: "#00A0A7",
                  borderRadius: "5px",
                }}
              >
                Save Changes
              </button>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Schedule;
