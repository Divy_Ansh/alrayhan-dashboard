import React, { useState, useEffect } from "react";

// components
import Loader from "../../Loader/Loader";
import ScrollableFeed from "react-scrollable-feed";

// helpers
import { getCompletedOrders } from "../../helpers/orderHelper";
import { isAuthenticated } from "../../helpers/AuthHelper";

const PendingOrders = ({ search }) => {
  // useState hooks
  const [Orders, setOrders] = useState([]);
  const [Reload, setReload] = useState(false);
  const [Loading, setLoading] = useState(true);

  // getAllOrderRequests
  const getAllCompleteOrders = () => {
    getCompletedOrders()
      .then((data) => {
        console.log("Data", data);
        setOrders(data);
        setLoading(false);
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  };

  // useEffect
  useEffect(() => {
    getAllCompleteOrders();
  }, [Reload]);

  // component return
  return (
    <div>
      {Loading ? (
        <div
          style={{
            width: "100%",
            minHeight: "80vh",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="col-md-10"
        >
          <Loader />
        </div>
      ) : (
        <div>
          <div className="order-request-cards-wrapper">
            {Orders &&
              Orders.filter((data) => {
                if (search && search.toLowerCase() === "") {
                  return data;
                } else if (data._id.includes(search.toLowerCase())) {
                  return data;
                }
              }).map((order) => {
                const {
                  _id,
                  customerNote,
                  totalAmount,
                  DeliveryType,
                  orderItems,
                  paymentType,
                } = order;
                return (
                  <ScrollableFeed>
                    <div
                      style={{
                        display: "grid",
                        gridTemplateColumns: "2fr 2fr 1fr",
                        alignItems: "center",
                        textAlign: "center",
                        boxShadow: "0 0 4px rgba(0,0,0,0.4)",
                        marginTop: "10px",
                        padding: "10px",
                      }}
                      className="order-request-card"
                      key={_id}
                    >
                      <div style={{ textAlign: "left" }}>
                        <h4>Order id #{_id}</h4>
                        <div style={{ width: "60%" }}>
                          {orderItems.length > 1 ? (
                            <h4
                              style={{
                                padding: "10px",
                                border: "1px dotted",
                                backgroundColor: "#EDEDED",
                              }}
                            >
                              {orderItems[0].name}+{orderItems.length - 1}
                            </h4>
                          ) : (
                            <h4
                              style={{
                                padding: "10px",
                                border: "1px dotted",
                                backgroundColor: "#EDEDED",
                              }}
                            >
                              {orderItems && orderItems[0].name}
                            </h4>
                          )}
                        </div>
                        <h4>{DeliveryType}</h4>
                      </div>

                      <div>
                        <h6 className="text-secondary">Customer Note:</h6>
                        <h4>{customerNote}</h4>
                      </div>
                      <div>
                        <h5>Delivery Time</h5>
                        <h5>$ {totalAmount}</h5>
                        <h5>{paymentType == "COD" ? "COD" : " Prepaid"}</h5>
                        <h5>Delivery Status</h5>
                      </div>
                    </div>
                  </ScrollableFeed>
                );
              })}
          </div>
        </div>
      )}
    </div>
  );
};

export default PendingOrders;
